<div>
    <div class="row justify-content-center align-items-center mx-0 px-0">
        <div class="col-12 col-md-8 col-lg-6 col-xl-5 my-5">
          <div class="card bg-dark text-white" style="border-radius: 1rem;">
            <div class="card-body p-5 text-center">
                <h2 class="fw-bold mb-2 text-uppercase">{{__('ui.etichettainserisci')}}</h2>
                <form wire:submit.prevent="createListing">
                    @csrf

                    <div class="form-outline form-white mb-4">
                        <label for="name" class="fw-semibold">{{__('ui.etichettanome')}}<span class="text-danger fw-bold">*</span></label>
                        <input type="text" wire:model="title" class="form-control form-control-sm" id="name" placeholder="Nome" value="{{old('title')}}">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-outline form-white mb-4">
                        <label for="price" class="fw-semibold">{{__('ui.etichettaprezzo')}}<span class="text-danger fw-bold">*</span></label>
                        <input type="text" wire:model="price" class="form-control form-control-sm" id="price" placeholder="es.50&euro;" value="{{old('price')}}">
                        @error('price')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <label for="category" class="fw-semibold">{{__('ui.etichettacategoria')}}<span class="text-danger fw-bold">*</span></label>
                    <select wire:model.defer="category" id="category" class="form-select mb-4">
                        <option value=" ">{{__('ui.etichettaselezione')}}</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{__('message.'.$category->category)}}</option>
                        @endforeach
                    </select>
                    @error('category')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
    
                    <div class="form-outline form-white mb-4">
                        <label for="exampleFormControlTextarea" class="fw-semibold">{{__('ui.etichettadescrizione')}}<span class="text-danger fw-bold">*</span></label>
                        <textarea class="form-control form-control-sm" wire:model="description" id="exampleFormControlTextarea" rows="3" placeholder="Descrizione del prodotto..." value="{{old('description')}}"></textarea>
                        @error('description')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form-outline form-white mb-4">
                        <label for="FormControlImage" class="fw-semibold">{{__('ui.etichettainserisciimmagini')}}</label>
                        <input wire:model="temporary_images" type="file" name="images" id="FormControlImage" multiple class="form-control form-control-sm shadow @error('temporary_images.*') is-invalid @enderror" placeholder="Img"/>
                        @error('temporary_images.*')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    @if (!empty($images))
                    <div class="row">
                        <div class="col-12">
                            <p class="fw-semibold">{{__('ui.etichettaanteprimaimmagini')}}</p>
                            <div class="row border-yellow rounded shadow py-4">
                                @foreach ($images as $key => $image)
                                <div class="col my-3">
                                        <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                                        <button type="button" class="btn btn-danger shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">{{__('ui.tastocancellaimmagine')}}</button>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                    <p class="mt-3" style="font-size:0.90rem;">{{__('ui.etichettacampi1')}}<span class="text-danger fw-bold">*</span>{{__('ui.etichettacampi2')}}</p>
                    <button type="submit" class="btn yellow font-title mt-3 zoomButton" style="color:black;">{{__('ui.tastoinserisci')}}</button>
                </form>
            </div>
          </div>
        </div>
      </div>
</div>
