<x-layout>
    <x-slot name="title">Presto - Annunci da revisionare</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container my-0">
        <div class="row mx-0 px-0">
          <h3 class="text-center my-2 p-0 fw-semibold text-uppercase font-title" style="font-size:2.5rem;">{{__('ui.pannellorevisore')}}</h3>
          <h4 class="text-center my-2 p-0 fw-semibold text-uppercase font-title">{{__('ui.ultima_modifica')}}</h4>
          <div class="d-flex align-items-center col-1 bg-dark text-center d-none d-lg-block" style="max-width=100%">
            <p class="my-2 text-white fw-bold">{{__('ui.immagine')}}</p>
          </div>
          <div class="d-flex align-items-center col-5 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.titolo')}}</p>
          </div>  
          <div class="d-flex align-items-center col-3 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.categoria')}}</p>
          </div> 
          <div class="d-flex align-items-center col-2 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.data_rev')}}</p>
          </div> 
          <div class="d-flex align-items-center col-1 bg-dark d-none d-lg-block text-center">
            <p class="my-2 text-white fw-bold">{{__('ui.annulla')}}</p>
          </div> 
          <hr> 
          @if($listingToReview)
          <div class="col-12 col-sm-12 col-lg-1 bg-white text-center mt-2 mt-lg-0">
            <img src="{{!$listingToReview->images()->get()->isEmpty() ? $listingToReview->images()->first()->getUrl(400,400) : "/image/default.png"}}" class="image-fluid" style="width:80%;">
          </div>
          <div class="d-flex align-items-center col-12 col-sm-12 col-lg-5 bg-white">
            <p class="mb-0 text-dark fw-bold">{{$listingToReview->title}}</p>
          </div>  
          <div class="d-flex align-items-center col-12 col-sm-12 col-lg-3 bg-white">
            <p class="mb-0 text-dark fw-bold">{{__('message.'.$listingToReview->category->category)}}</p>
          </div> 
          <div class="d-flex align-items-center col-12 col-sm-12 col-lg-2 bg-white">
            <p class="mb-0 text-dark fw-bold">{{__('ui.revisionato')}} {{$listingToReview->updated_at->format('d/m/Y')}}</p>
          </div> 
          <div class="d-flex align-items-center col-12 col-sm-12 col-lg-1 bg-white mb-2 mb-lg-0 d-flex justify-content-center">
            <form action="{{route('undo' , ['listing'=>$listingToReview])}}" method="POST">
              @csrf
              @method('PATCH')
              <button class="card-button rounded text-decoration-none border-0" style="width:40px; height:40px;" type="submit"><i class="fa-solid fa-rotate-left"></i></button>
            </form>
          </div>  
          @else
            <div class="col-12 alert alert-warning">
              <p class="fw-semibold m-0">{{__('ui.norevisioni')}}</p>
            </div>     
          @endif
        <div class="row mx-0 px-0 mt-3">
          {{-- <h3 class="text-center my-2 p-0 fw-semibold text-uppercase font-title" style="font-size:2.5rem;">{{__('ui.pannellorevisore')}}</h3> --}}
          <h4 class="text-center my-2 p-0 fw-semibold text-uppercase font-title">{{__('ui.annunci_da')}}</h4>
          <div class="d-flex align-items-center col-1 bg-dark text-center d-none d-lg-block" style="max-width=100%">
            <p class="my-2 text-white fw-bold">{{__('ui.immagine')}}</p>
          </div>
          <div class="d-flex align-items-center col-5 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.titolo')}}</p>
          </div>  
          <div class="d-flex align-items-center col-3 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.categoria')}}</p>
          </div> 
          <div class="d-flex align-items-center col-2 bg-dark d-none d-lg-block">
            <p class="my-2 text-white fw-bold">{{__('ui.data')}}</p>
          </div> 
          <div class="d-flex align-items-center col-1 bg-dark d-none d-lg-block text-center">
            <p class="my-2 text-white fw-bold">{{__('ui.verifica')}}</p>
          </div> 
          <hr>
          <div class="row mx-0 px-0">
            @forelse($allNotyet as $singleNotyet)  
              <div class="col-12 col-sm-12 col-lg-1 bg-white text-center mt-2 mt-lg-0">
                <img src="{{!$singleNotyet->images()->get()->isEmpty() ? $singleNotyet->images()->first()->getUrl(400,400) : "/image/default.png"}}" class="image-fluid" style="width:80%;">
              </div>
              <div class="d-flex align-items-center col-12 col-sm-12 col-lg-5 bg-white">
                <p class="mb-0 text-dark fw-bold">{{$singleNotyet->title}}</p>
              </div>  
              <div class="d-flex align-items-center col-12 col-sm-12 col-lg-3 bg-white">
                <p class="mb-0 text-dark fw-bold">{{__('message.'.$singleNotyet->category->category)}}</p>
              </div> 
              <div class="d-flex align-items-center col-12 col-sm-12 col-lg-2 bg-white">
                <p class="mb-0 text-dark fw-bold">{{__('ui.dataArticolo')}} {{$singleNotyet->created_at->format('d/m/Y')}}</p>
              </div> 
              <div class="d-flex align-items-center col-12 col-sm-12 col-lg-1 bg-white mb-2 mb-lg-0 d-flex justify-content-center">
                <a href="{{route('showSingleListingToBeRevisioned', compact('singleNotyet'))}}" class="card-button rounded text-decoration-none" style="width:40px; height:40px;" title="{{__('ui.verifica')}}"><i class="fa-solid fa-square-check"></i></a>
              </div>    
              <hr>     
            @empty
              <div class="box">
                <div class="col-12 alert alert-warning">
                  <p class="fw-semibold m-0">{{__('ui.norevisioni')}}</p>
                </div>     
              </div> 
            @endforelse
          </div>              
          <div class="col-12">
            {{$allNotyet->links()}}   
          </div>
          <div class="box"></div>
        </div>  
      </div>
    </div>
</x-layout>