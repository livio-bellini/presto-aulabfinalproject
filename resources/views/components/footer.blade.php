<footer class="text-center text-lg-start bg-footer text-light">
    <!-- Section: Social media -->
    <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
      <!-- Left -->
      <div class="me-5 d-none d-lg-block">
        <span class= "fw-bold">{{__('ui.fraseTOP')}}</span>
      </div>
      <!-- Left -->
  
      <!-- Right -->
      <div>
        <a href="" class="me-4 text-reset text-decoration-none">
          <i class="fa-brands fa-facebook"></i>
        </a>
        <a href="" class="me-4 text-reset text-decoration-none">
          <i class="fa-brands fa-instagram"></i>
        </a>
        <a href="" class="me text-reset text-decoration-none">
          <i class="fa-brands fa-linkedin"></i>
        </a>
      </div>
      <!-- Right -->
    </section>
    <!-- Section: Social media -->
  
    <!-- Section: Links  -->
    <section class="">
      <div class="container text-center text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            <!-- Content -->        
            <img width="70" height="70" class="rounded" src="/image/logoPresto.png" alt="Logo del sito.">
            <p class= fst-italic">
              {{__('ui.fraseBOT')}}
            </p>
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="fw-bold mb-4">
              {{__('ui.servizi')}}
            </h6>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('whoPage')}}">{{__('ui.chi')}}</a>
            </p>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('tipsPage')}}">{{__('ui.consigli')}}</a>
            </p>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('safetyPage')}}">{{__('ui.sicurezza')}}</a>
            </p>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('rulesPage')}}">{{__('ui.regole')}}</a>
            </p>
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="fw-bold mb-4">
              {{__('ui.altrilinks')}}
            </h6>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('mapPage')}}">{{__('ui.mappa')}}</a>
            </p>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('privacyPage')}}">{{__('ui.condizioni')}}</a>
            </p>
            <p>
              <a class="text-decoration-none text-light"  href="{{route('ambientPage')}}">{{__('ui.ambiente')}}</a>
            </p>
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h1><a class="text-decoration-none text-light" href="{{route('homePage')}}">Presto.it<a></h1>
              <p>
                <a class="text-decoration-none zoomButton text-dark btn yellow fw-bold rounded text-uppercase fs-5"  href="{{route('writeMailBecome')}}">{{__('ui.lavora')}}</a>
              </p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->
  
    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
      <a class="text-reset fw-bold text-decoration-none" href="">© 2023 Copyright: Presto.it</a>
    </div>
    <!-- Copyright -->
  </footer>