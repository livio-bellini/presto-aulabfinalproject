<form action="{{route('langSystem', compact('lang'))}}" method="POST">
    @csrf
    <button type="submit" class="nav-link border-0 fs-5 bg-dark">
        <div class="flag-icon flag-icon-{{$nation}}"></div>
    </button>
</form>