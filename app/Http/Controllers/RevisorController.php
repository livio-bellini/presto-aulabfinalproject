<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Listing;
use App\Mail\BecomeRevisor;
use Illuminate\Http\Request;
use App\Mail\AspiringRevisor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use App\Mail\ConfirmationRevisor;

class RevisorController extends Controller
{
    public function indexListing(){
        $allNotyet=Listing::where('is_accepted',null)->paginate(25);
        $listingToReview=Listing::whereNotNull('is_accepted')->orderBy('updated_at' , 'desc')->first();
        return view('revisor.index', compact('allNotyet', 'listingToReview'));
    }

    public function showSingleNotYet(Listing $singleNotyet){
        return view('revisor.show', compact('singleNotyet'));
    }

    public function acceptListing(Listing $listing){
        $listing->setAccepted(true);
        $singleNotyet = Listing::where('is_accepted',null)->where('id', '>', $listing->id)->first();
        if($singleNotyet != null){
          return redirect()->route('showSingleListingToBeRevisioned', compact('singleNotyet'))->with('message', __('message.annuncioconvalidato'));
        } else if($singleNotyet == null){
          return redirect()->route('indexRevisor')->with('message', __('message.annuncioconvalidato'));
        }
    }
    
    public function rejectListing(Listing $listing){
        $listing->setAccepted(false);
        $singleNotyet = Listing::where('is_accepted',null)->where('id', '>', $listing->id)->first();
        if($singleNotyet != null){
          return redirect()->route('showSingleListingToBeRevisioned', compact('singleNotyet'))->with('message', __('message.annunciorifiutato'));
        } else if($singleNotyet == null){
          return redirect()->route('indexRevisor')->with('message', __('message.annunciorifiutato'));
        }
    }

    public function becomeWorker(){
        return view('work-with-us');
    }

    public function sendMailWorker(Request $request){
        ['description'=>$request->description];
        $data = $request->description;
        Mail::to('admin@presto.it')->send(new BecomeRevisor($data , Auth::user()));
        Mail::to(Auth::user()->email)->send(new AspiringRevisor(Auth::user()));
        return redirect('/')->with('message', __('message.candidaturainviata')); 
    }

    public function makeRevisor(User $user){
        if(Auth::user()){
          Artisan::call('presto:MakeUserRevisor', ["email"=>$user->email]);
          Mail::to(Auth::user()->email)->send(new ConfirmationRevisor(Auth::user()));
          return redirect('/')->with('message', __('message.seirevisore'));      
        }else if(!Auth::user()){
          return redirect('/')->with('message', __('message.essereloggato'));   
        }
    }

    public function undoListing(Listing $listing){
        $listing->setAccepted(null);
        return redirect(route('indexRevisor'))->with('message', __('message.modificaannullata'));
    }
}