<x-layout>
    <x-slot name="title">Presto - Lavora con noi</x-slot>
    <div class="container-fluid vh-auto">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="row justify-content-center px-2 px-sm-3 px-md-4 px-lg-5 px-xl-0">
        <div class="card my-5 bg-dark px-0" style="max-width: 961px;">
          <div class="row justify-content-between align-items-center g-0">
            <div class="col-md-6 col-sm-12 col-12 align-self-md-start align-selflg-center">
              <img class="img-fluid rounded" src="/image/LavoraConNoi.jpg" style="width:100%;" alt="Ragazzi del team a lavoro.">
            </div>
            <div class="col-md-6 col-sm-12 col-12">
              <div class="card-body row text-white text-center ">              
                <h2 class="font-title fw-bold mb-3 text-uppercase">{{__('ui.workTitle1')}}</h2>
                <p class="font-standard text-white-50 mb-2 text-center" style="font-size:0.9rem;">{{__('ui.workCiao')}}{{Auth::user()->name}}{{__('ui.workText')}}</p>
                <form method="POST" action="{{route('sendMailBecome')}}">
                  @csrf
                  <div class="col-12">
                    <label for="exampleInputText" class="font-standard form-label">{{__('ui.workTitle2')}}</label>
                    <textarea id="exampleInputText" name="description" cols="70" style="max-width:100%;" rows="7"></textarea>
                  </div>
                  <button type="submit" class="btn yellow mt-3 mb-3 fw-bold" style="color:black;">{{__('ui.workButton')}}</button> 
                </form>                                    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</x-layout>



