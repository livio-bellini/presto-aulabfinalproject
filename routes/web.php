<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ListingController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Controller generico
Route::get('/', [PublicController::class, 'home'])->name('homePage');
Route::get('/categoria/{category}', [PublicController::class, 'show'])->name('showCategory');
Route::get('/ricerca-annuncio', [PublicController::class, 'searchLstngs'])->name('searchListings');
Route::get('/chiSiamo', [PublicController::class, 'who'])->name('whoPage');
Route::get('/mappa', [PublicController::class, 'map'])->name('mapPage');
Route::get('/condizioni', [PublicController::class, 'privacy'])->name('privacyPage');
Route::get('/consigli', [PublicController::class, 'tips'])->name('tipsPage');
Route::get('/sicurezza', [PublicController::class, 'safety'])->name('safetyPage');
route::get('/regole', [PublicController::class, 'rules'])->name('rulesPage');
Route::get('/ambiente', [PublicController::class, 'ambient'])->name('ambientPage');
Route::post('/lingua/{lang}', [PublicController::class, 'language'])->name('langSystem');


// Controller annunci
Route::get('/annunci', [ListingController::class, 'index'])->name('allListing');
Route::get('/inserisci-annuncio', [ListingController::class, 'create'])->name('createListing');
Route::get('/dettaglio-annuncio/{listing}', [ListingController::class, 'show'])->name('showListing');

// Controller revisore
Route::get('/revisione-annuncio', [RevisorController::class, 'indexListing'])->middleware('isRevisor')->name('indexRevisor');
Route::get('/annuncio-sotto-revisione/{singleNotyet}', [RevisorController::class, 'showSingleNotYet'])->name('showSingleListingToBeRevisioned');
Route::patch('/annuncio-accettato/{listing}', [RevisorController::class, 'acceptListing'])->middleware('isRevisor')->name('accepted');
Route::patch('/annuncio-rifiutato/{listing}', [RevisorController::class, 'rejectListing'])->middleware('isRevisor')->name('rejected');
Route::patch('/annuncio-modificato/{listing}', [RevisorController::class, 'undoListing'])->middleware('isRevisor')->name('undo');

// Mail 
Route::get('/lavora-con-noi', [RevisorController::class, 'becomeWorker'])->middleware('auth')->name('writeMailBecome');
Route::post('/invia-mail-assunzione', [RevisorController::class, 'sendMailWorker'])->name('sendMailBecome');
Route::get('/rendi-revisore/{user}', [RevisorController::class, 'makeRevisor'])->name('confirmRevisor');