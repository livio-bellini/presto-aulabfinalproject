<x-layout>
    <x-slot name="title">Presto - Registrati</x-slot>
        <div class="container-fluid vh-100">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5 my-5">
              <div class="card bg-dark text-white" style="border-radius: 1rem;">
                <div class="card-body p-5 text-center">
      
                  
      
                    <h2 class="fw-bold mb-2 text-uppercase">{{__('ui.linkAregistrazione')}}</h2>
                    <p class="text-white-50 mb-5">{{__('ui.fraseRegistrazione1')}}</p>
                    <form method="POST" action="{{route('register')}}">

                        @csrf

                        <div class="form-outline form-white mb-4">
                            <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" placeholder="Mario"/>
                            <label class="form-label">{{__('ui.registrazioneNome')}}</label>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-outline form-white mb-4">
                        <input type="email" name="email" id="typeEmailX" class="form-control form-control-sm @error('email') is-invalid @enderror" placeholder="email@email.com"/>
                        <label class="form-label" for="typeEmailX">Email</label>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
        
                        <div class="form-outline form-white mb-4">
                        <input type="password" name="password" class="form-control form-control-sm @error('password') is-invalid @enderror" placeholder="**********"/>
                        <label class="form-label">{{__('ui.REGLOGpassword')}}</label>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>

                        <div class="form-outline form-white mb-4">
                            <input type="password" name="password_confirmation"  class="form-control form-control-sm @error('password_confirmation') is-invalid @enderror" placeholder="**********"/>
                            <label class="form-label">{{__('ui.registrazioneConferma')}}</label>
                            @error('password_confirmation')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                
                        <button class="btn btn-outline btn-sm px-5  yellow zoomButton fw-bold rounded fs-5 mb-1 text-uppercase" type="submit" style="color:black;">{{__('ui.registrati')}}</button>
      
                    </form>
      
                  
      
                  <div>
                    <p class="mb-0">{{__('ui.fraseRegistrazione2')}} <a href="{{route('login')}}" class="text-white-50 fw-bold">{{__('ui.linkAlogin')}}</a>
                    </p>
                  </div>
      
                </div>
              </div>
            </div>
          </div>
        </div>

</x-layout>