<x-layout>
    <x-slot name="title">Presto - Regolamento</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-12 col-md-10">
            <h4 class="display-4 mt-5 pt-4 fw-semibold">{{__('ui.normativa')}}</h4>
            <h4 class="display-4 mb-3 fw-semibold">{{__('ui.regoleTitolo0')}}</h4>
            <p class="fs-4 mb-3 p-0">{{__('ui.lanormativa')}}</p>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-12 col-sm-12 col-md-10">
            <h3 class="text-center display-4 mb-3 pt-4 mt-4  fw-semibold">{{__('ui.regoleTitolo1')}}</h3>
            <p class="fs-4 mb-3 p-0">{{__('ui.regoleTesto1.1')}}</p>
            <p class="fs-4 mb-3 p-0">{{__('ui.regoleTesto1.2')}}</p>
          </div>
        </div>
        <div class="row justify-content-center mt-2">  
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-camera-retro fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.regoleTitolo2')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.regoleTesto2')}}</p>
          </div>
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-euro-sign fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.regoleTitolo3')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.regoleTesto3')}}</p>
          </div>
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-bars-staggered fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.regoleTitolo4')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.regoleTesto4')}}</p>
          </div>
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-check fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.regoleTitolo5')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.regoleTesto5')}}</p>
          </div>
        </div>
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{route('homePage')}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>        
    </div>
</x-layout>