//LOADING BAR
let loading = document.querySelector('#progress');
function spinner() {
    setTimeout(function () {
        loading.classList.add('d-none');
    }, 1280);
};
spinner();

//READING DARK MODE STATUS FROM BROWSER STORAGE
let btnSwitchdarkmode = document.querySelector('#btnDarkMode');
let visualMode = localStorage.getItem('visualMode');
if (visualMode == 'light'){
  document.documentElement.style.setProperty('--main-color', '#EAE0D5');
  document.documentElement.style.setProperty('--dark', '#0A0908');
  btnSwitchdarkmode.innerHTML = `<i class="fa-solid fa-moon" style="color:var(--main-color); font-size:1.3rem;"></i>`;
} else if (visualMode == 'night'){
  document.documentElement.style.setProperty('--main-color', '#22333B');
  document.documentElement.style.setProperty('--dark', '#EAE0D5');
  btnSwitchdarkmode.innerHTML = `<i class="fa-solid fa-moon" style="color:var(--yellow); font-size:1.3rem;"></i>`;
}

//LIGHT and DARK MODE SWITCH BUTTON
let isToggle = true;
btnSwitchdarkmode.addEventListener('click', () => {
  console.log(isToggle)
  if (isToggle){
    document.documentElement.style.setProperty('--main-color', '#22333B');
    document.documentElement.style.setProperty('--dark', '#EAE0D5');
    btnSwitchdarkmode.innerHTML = `<i class="fa-solid fa-moon" style="color:var(--yellow); font-size:1.3rem;"></i>`;
    isToggle = false;
    localStorage.setItem('visualMode', 'night');
  } else {
    document.documentElement.style.setProperty('--main-color', '#EAE0D5');
    document.documentElement.style.setProperty('--dark', '#22333B');
    btnSwitchdarkmode.innerHTML = `<i class="fa-solid fa-moon" style="color:var(--main-color); font-size:1.3rem;"></i>`;
    isToggle = true;
    localStorage.setItem('visualMode', 'light');
  }
});

//LOGOUT SYSTEM
let buttonLogout = document.querySelector('#buttonLogout');
let formLogout = document.querySelector('#formLogout');
if (buttonLogout != null){
  buttonLogout.addEventListener('click', (event) => {
    event.preventDefault();
    formLogout.submit();
  });
}
//AUTOMATIC HIDING MESSAGE
let hideAlert = document.querySelector('.hideAlert');
if (hideAlert != null){
  setTimeout(() => {
    hideAlert.classList.add('d-none');
  }, 4950);
}

//AUTOMATIC FADE-IN OF MULTIPLE CARD FROM RIGHT
let boxesR = document.querySelectorAll('#boxEsR > *');
function handleIntersection2(entries2){
  entries2.map((entry, index) => {
    if(entry.isIntersecting){
      entry.target.classList.remove('opacity-0');
      entry.target.classList.add('slideCardFromRight');
      entry.target.style.animationDelay = `${index++}s`;
    }
  });
}
let observerBoxEsR = new IntersectionObserver(handleIntersection2);
if (boxesR != null){
  boxesR.forEach(element => {
      observerBoxEsR.observe(element);
  });
}


//AUTOMATIC FADE-IN CARD SINGLE CARD FROM LEFT
let slideCardL = document.querySelector('.slideCardL');
if(slideCardL != null){
  slideCardL.classList.add('slideCardFromLeft')
  slideCardL.classList.remove('opacity-0');
}

//AUTOMATIC FADE-IN CARD SINGLE CARD FROM RIGHT
let slideCardR = document.querySelector('.slideCardR');
if(slideCardR != null){
  slideCardR.classList.add('slideCardFromRight')
  slideCardR.classList.remove('opacity-0');
}

//SWIPER
let swiper = new Swiper(".mySwiper", {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
});
let swiper2 = new Swiper(".mySwiper2", {
  spaceBetween: 10,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  thumbs: {
    swiper: swiper,
  },
});

//SCROLL TO TOP BUTTON
const scrollBtn = document.querySelector('.topBtnYellow');
if(scrollBtn != null){
  document.addEventListener('scroll', () => {
    let winPosition = window.scrollY;
    if (winPosition == 0) {
      scrollBtn.classList.add("d-none");
    } else {
      scrollBtn.classList.remove("d-none");
      scrollBtn.addEventListener('click', () => {
        if (winPosition != 0) {
          setTimeout(function () {
            window.scrollTo({ top: 0, behavior: 'smooth' });          
          }, 200);
        }
      });
    }
  });
}

//ICONS GENERATOR FOR CATEGORIES IN HOME
const categoriesIcons = document.querySelectorAll('.categoriesHome');
if (categoriesIcons != null){
  for(let i = 0; i < categoriesIcons.length; i++){
    switch(i){
      case 0:
        categoriesIcons[0].innerHTML = `<i class="fa-solid fa-mobile-screen-button fa-2x colorIcon"></i>`;
        break;
      case 1:
        categoriesIcons[1].innerHTML = `<i class="fa-solid fa-film fa-2x colorIcon"></i>`;
        break;
      case 2:
        categoriesIcons[2].innerHTML = `<i class="fa-solid fa-chart-line fa-2x colorIcon"></i>`;
        break;
      case 3:
        categoriesIcons[3].innerHTML = `<i class="fa-solid fa-seedling fa-2x colorIcon"></i>`;
        break;
      case 4:
        categoriesIcons[4].innerHTML = `<i class="fa-solid fa-book-open fa-2x colorIcon"></i>`;
        break;
      case 5:
        categoriesIcons[5].innerHTML = `<i class="fa-solid fa-music fa-2x colorIcon"></i>`;
        break;
      case 6:
        categoriesIcons[6].innerHTML = `<i class="fa-solid fa-couch fa-2x colorIcon"></i>`;
        break;
      case 7:
        categoriesIcons[7].innerHTML = `<i class="fa-solid fa-volleyball fa-2x colorIcon"></i>`;
        break;
      case 8:
        categoriesIcons[8].innerHTML = `<i class="fa-solid fa-shirt fa-2x colorIcon"></i>`;
        break;
      case 9:
        categoriesIcons[9].innerHTML = `<i class="fa-solid fa-plane-departure fa-2x colorIcon"></i>`;
        break;
      default:
        break;
    }
  }
}

//AUTOMATIC RESIZING OF THE CARDS IN HOME 
let allHomeCards = document.querySelectorAll('.singleCard');
function controlWindowSize2(a){
  let windowWidth = window.innerWidth;
  a.forEach(b => {
    switch(true){
      case (windowWidth < 576):
        b.removeAttribute("style");
        b.setAttribute("style", "width:260px;");
        break;
      case (windowWidth >= 576 && windowWidth < 768):
        b.removeAttribute("style");
        b.setAttribute("style", "width:240px;");
        break;
      case (windowWidth >= 768):
        b.removeAttribute("style");
        b.setAttribute("style", "width:230px;");
        break;
      default:
        break;
    }
  })  
}
if (allHomeCards != null){
  controlWindowSize2(allHomeCards);
  window.addEventListener("resize", () => {
    controlWindowSize2(allHomeCards);
  });
}

//DYNAMIC CARDS OPACITY IN WELCOME PAGE AND CATEGORY PAGE
if (allHomeCards != null) {
  allHomeCards.forEach(singleCard => {
    singleCard.addEventListener('mouseover', () => {
      let myTime = setTimeout(function () {
        allHomeCards.forEach(singleCard => {
          singleCard.classList.remove('notransition');
          singleCard.classList.add('blurAllCards');
        });        
      }, 250);  
      singleCard.addEventListener('mouseleave', () => {
        clearTimeout(myTime);
        allHomeCards.forEach(singleCard => {
          singleCard.classList.add('notransition');
          singleCard.classList.remove('blurAllCards');
          singleCard.classList.remove('notransition');
        });
      });
    });
  });
}

//MAP SITE CARD SWITCH SYSTEM
let selectedSlide = 0;
const slides = document.querySelectorAll('.mySlides');
const dots = document.querySelectorAll('.dot');
const scrollLeft = document.querySelector('.scrollLeft');
const scrollRight = document.querySelector('.scrollRight');

// previous card button
if(scrollLeft != null){
  scrollLeft.addEventListener('click', () => {
    for(let i = 0; i < slides.length; i++){
      if(!slides[i].classList.contains('d-none')){
        selectedSlide = i - 1;
      };
    }
    showActualCard(selectedSlide);
  })
}

// next card button
if(scrollRight != null){
  scrollRight.addEventListener('click', () => {
    for(let i = 0; i < slides.length; i++){
      if(!slides[i].classList.contains('d-none')){
        selectedSlide = i + 1;
      };
    }
    showActualCard(selectedSlide);
  })
}

if(dots != null){
  dots.forEach(dot => {
    dot.addEventListener('click', () => {
      selectedSlide = dot.textContent;
      showActualCard(selectedSlide);
    });
  });
}

if((slides != null) && (dots != null) && (scrollLeft != null) && (scrollRight != null)){
  showActualCard(selectedSlide);
}

function showActualCard(a){
  if(slides != null){
    slides.forEach(slide => {
      slide.classList.add('d-none');
    })
    dots.forEach(dot => {
      dot.classList.remove('active2');
    })
  if(a >= (slides.length-1)){
    scrollRight.classList.add('d-none');
  } else if(a < slides.length){
    scrollRight.classList.remove('d-none');
  }
  if(a <= 0){
    scrollLeft.classList.add('d-none');
  } else if(a > 0){
    scrollLeft.classList.remove('d-none');
  }
  dots[a].classList.add('active2');
  slides[a].classList.remove('d-none');
  }
}