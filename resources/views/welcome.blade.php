<x-layout>
    <x-slot name="title">Presto - Home</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="row my-0 mx-0 justify-content-center gradient-custom-home">
        @foreach ($categories as $category)
          <div class="col-5 col-sm-4 col-md-3 col-lg-1 card text-center border-0" style="background-color:transparent">
            <div class="card-body px-0">
              <a class="categoriesHome" href="{{route('showCategory', compact('category'))}}"></a>
              <p class="text-dark fw-bold" style="font-size:0.8rem">{{__('message.'.($category->category))}}</p>
            </div>
          </div>
        @endforeach         
      </div>
      <div class="container">
        <div class="row px-3 mt-5 px-md-2 justify-content-md-center">
          <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-2 px-0 ms-lg-5 d-block d-md-none">
            @if(Auth::check())
              <a class="btn yellow zoomButton fw-bold rounded" style="width:100%;" href="{{route('createListing')}}">{{__('ui.nuovo')}}</a>
            @else
              <a class="btn yellow zoomButton fw-bold rounded" style="width:100%;" href="{{route('login')}}">{{__('ui.login')}}</a>
            @endif
          </div>
          <form class="d-block d-lg-none col-12 col-sm-12 col-md-8 mt-2 mt-md-0  px-0" action="{{route('searchListings')}}" method="GET">
            <div class="row px-0 mx-0"> 
              <div class="col-10 px-0">
                  <input name="searched" class="form-control angleLeft" style="width:100%;" type="search" placeholder="{{__('ui.cerca')}}" aria-label="search"> 
              </div>
              <div class="col-2 px-0">
                  <button class="btn yellow  angleRight text-center px-0" style="width:100%;" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button> 
              </div>  
            </div>                  
          </form>
        </div>
      </div>
      <div class="container-fluid bg-header-img">
        <div class="row w-100">
          <div class="col-6 d-none d-md-block ms-5 pt-5">
            <div class="vh-40 d-flex align-items-center">
              <div class="card slideCardL opacity-0" style="width:35rem;">
                <div class="card-body">         
                  <p class="fw-bold text-dark fs-2">{{__('ui.fraseHomecard1')}}</p>
                  <p class="fw-bold text-dark fs-4">{{__('ui.fraseHomecard2')}}</p>  
                  <div class="row w-100 justify-content-center">
                    @if(Auth::check())
                      <a class="btn yellow zoomButton fw-bold rounded" style="width:40%;" href="{{route('createListing')}}">{{__('ui.nuovo')}}</a>
                    @else
                     <a class="btn yellow zoomButton fw-bold rounded" style="width:40%;" href="{{route('login')}}">{{__('ui.login')}}</a>
                    @endif
                  </div>                
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex align-items-end justify-content-end mt-5">
            {{-- <h3 class="my-4 p-0 fw-bold font-title" style="font-size:2.5rem;">Presto.it</h3> --}}
            <img src="/image/logoPresto.png" width="120" height="120" alt="">
          </div>
        </div>
      </div>

      <div class="container mt-5">
        @if(session('error'))
            <div class="alert alert-danger hideAlert">
                {{session('error')}}
            </div>
        @endif
        <div class="row px-0">
          
          @foreach($listings as $listing) 
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 px-0 justify-content-center d-flex">
              <div class="card shadow singleCard px-0 m-2">
                <div class="card-image text-center">
                  <img src="{{!$listing->images()->get()->isEmpty() ? $listing->images()->first()->getUrl(400,400) : "/image/default.png"}}" class="image-fluid">
                </div>
                <div class="card-body">
                  <p class="card-heading text-dark fw-bold font-standard m-0 p-0 text-truncate d-inline-block" style="max-width:100%;">{{$listing->title}}</p>
                  <p class="card-title text-dark font-standard p-0 m-0">{{$listing->category->category}}</p>
                  <p class="card-text text-dark font-standard m-0 p-0">Inserito il: {{$listing->created_at->format('d/m/Y')}}</p>
                </div>
                <a href="{{route('showListing', compact('listing'))}}" class="card-button fw-bold text-decoration-none">{{__('ui.dettagli')}}</a>
              </div>
            </div>
          @endforeach
          <div class="d-none">
            {{$listings->links()}}
          </div>
        </div>  
      </div>
      <div class="container vh-auto">
        <div class="row justify-content-center px-2 px-sm-3 px-md-4 px-lg-5 px-xl-0">
          <div class="card my-5 px-0" style="max-width: 650px;">
            <div class="row justify-content-between align-items-center g-0">
              <div class="col-md-6 col-sm-12 col-12 align-self-md-start align-selflg-center">
                <img class="img-fluid rounded" src="/image/ambiente.jpg" style="width:100%;" alt="Germoglio dentro una lampadina.">
              </div>
              <div class="col-md-6 col-sm-12 col-12">
                <div class="card-body row text-start mx-0 px-0">                     
                  <h3 class="fw-bold mb-3 text-center text-md-start text-dark">{{__('ui.ambientCardTitle')}}</h3>
                  <p class="mb-2 text-center text-md-start text-dark fs-6" style="font-size:0.78rem;">{{__('ui.ambientCardText')}}</p>
                  <div class="col-6 w-100 text-center">
                    <a href="{{route('ambientPage')}}" class="btn yellow my-3 fw-bold" style="color:black;">{{__('ui.continueCardButton')}}</a>
                  </div>
                </div>                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</x-layout>












