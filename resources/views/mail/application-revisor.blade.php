<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presto.it</title>
</head>
<body>
    <div>
        <h1>RICHIESTA DI ASSUNZIONE</h1>
        <h2>Il seguente utente ha presentato domanda di assunzione come revisore.</h2>
        <p>Nome: {{$user->name}}</p>
        <p>Email: {{$user->email}}</p>
        <p>Presentazione: {{$data}}</p>
        <a href="{{route('confirmRevisor', compact('user'))}}">RENDI REVISORE</a>
    </div>
</body>
</html>