<nav class="navbar py-0 navbar-expand-lg bg-dark">
  <div class="container-fluid px-1">
    <a class="navbar-brand mx-0" href="{{route('homePage')}}"><img width="50" height="50" class="rounded" src="/image/logoPresto.png" alt="Logo di Presto.it."><a>
    <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="toggler-icon top-bar"></span>
      <span class="toggler-icon middle-bar"></span>
      <span class="toggler-icon bottom-bar"></span>
    </button>
    <div class="collapse row mx-0 px-0 navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav col-12 col-lg-3 px-0 mx-0  mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active text-light fw-bold" aria-current="page" href="{{route('allListing')}}">{{__('ui.annunci')}}</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link text-light fw-bold dropdown-toggle" href="#" id="categoriesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{__('ui.categoria')}}
            </a>
            <ul class="dropdown-menu" aria-labelledby="categoriesDropdown">
              @foreach ($categories as $category)
                <li class="colorOption"><a class="dropdown-items btn text-dark fw-bold text-truncate" href="{{route('showCategory', compact('category'))}}">
                  {{__('message.'.($category->category))}}</a>
                </li>
                <li><hr class="dropdown-divider"></li>
              @endforeach
            </ul>
          </li>
        </ul>
        <form class="d-none d-lg-block col-lg-5 col-xl-6 px-0" action="{{route('searchListings')}}" method="GET">
          <div class="row px-0 mx-0"> 
            <div class="col-9 px-0">
                <input name="searched" class="form-control angleLeft text-dark fw-bold" style="width:100%;" type="search" placeholder="{{__('ui.cerca')}}" aria-label="search"> 
              </div>
              <div class="col-1 px-0">
                  <button class="btn yellow angleRight text-center px-0" style="width:100%;" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button> 
              </div>  
          </div>                  
        </form>
        <div class="col-12 col-lg-4 col-xl-3 d-flex flex-row justify-content-lg-end px-0 mx-0">
          <ul class="navbar-nav mb-2 mb-lg-0">
            <button id="btnDarkMode" type="button" class="btn text-start order-2 order-lg-1 mt-lg-1">
              <i class="fa-solid fa-moon" style="color:var(--main-color); font-size:1.3rem;"></i>
            </button>
            @if(Session::get('locale') == 'it')
              <li style="border:2px solid var(--yellow);"><x-language lang="it" nation="it"/></li>
            @else
              <li><x-language lang="it" nation="it"/></li>
            @endif
            @if(Session::get('locale') == 'gb')
              <li style="border:2px solid var(--yellow);"><x-language lang="gb" nation="gb"/></li>
            @else
              <li><x-language lang="gb" nation="gb"/></li>
            @endif
            @if(Session::get('locale') == 'es')
              <li style="border:2px solid var(--yellow);"><x-language lang="es" nation="es"/></li>
            @else
              <li><x-language lang="es" nation="es"/></li>
            @endif
            <li class="nav-item dropdown order-1 order-lg-3">
              <a class="nav-link dropdown-toggle text-capitalize text-light fw-bold mt-lg-1" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                @auth                
                  {{__('ui.saluto')}} {{Auth::user()->name}}
                  @if(Auth::user()->is_revisor)
                    <span class="badge rounded-pill bg-danger">
                      {{App\Models\Listing::toBeRevisionedCount()}} 
                    </span>
                  @endif
                @else
                  {{__('ui.accesso')}}
                @endauth
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                @guest
                  <li><a class="dropdown-item colorOption fw-bold" href="{{route('login')}}">{{__('ui.login')}}</a></li>
                  <li><a class="dropdown-item colorOption fw-bold" href="{{route('register')}}">{{__('ui.registrati')}}</a></li>
                @else
                  @if(Auth::user()->is_revisor)
                    <li><a class="dropdown-item colorOption fw-bold" href="{{route('indexRevisor')}}">{{__('ui.pannellorevisore')}}</a></li>
                  @endif
                  <li><button id="buttonLogout" class="dropdown-item colorOption fw-bold">{{__('ui.logout')}}</button></li>
                  <form id="formLogout" method="POST" action="{{route('logout')}}">@csrf</form>
                @endguest
              </ul>
            </li>
          </ul>  
        </div>
    </div>     
  </div>
  <div id="progress" class="loadBar my-0 py-0"></div>
</nav>