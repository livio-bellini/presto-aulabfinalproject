<x-layout>
    <x-slot name="title">Presto - Sicurezza</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container-fluid bg-safety-img">
        <div class="row vh-40 justify-content-center align-items-end">
          <div class="col-12 mt-0">
            {{-- <h1 class="text-center display-3 my-3 p-0 text-dark fw-semibold text-uppercase font-title">{{__('ui.sicurezza')}}</h1>  --}}
          </div> 
        </div>
      </div>
      <div class="container my-4">
        <div class="row justify-content-center ">
          <div class="col-10">
            <h3 class="text-center display-3 mb-3 p-0 fw-semibold display-5">{{__('ui.sicurezzaTitolo1')}}</h3>
          </div>
          <div class="col-12 col-lg-10">
            <h5 class="display-3 mb-3 p-0 fw-semibold display-6">{{__('ui.sicurezzaTitolo2')}}</h5>
            <p class=" fs-3 display-3 mb-3 p-0">{{__('ui.sicurezzaTesto1')}}</p>
          </div>
        </div>
        <div class="row justify-content-center mt-5">
          <div class="col-12 col-lg-10">
            <h5 class="display-3 mb-3 p-0 fw-semibold display-6 ">{{__('ui.sicurezzaTitolo3')}}</h5>
          </div>  
        </div>
        <div class="row justify-content-center mt-2">  
          <div class="col-12 col-lg-3">
            <i class="fa-sharp fa-solid fa-magnifying-glass fa-2x"></i>
            <h4 class="fs-3 mb-3 p-0 fw-semibold ">{{__('ui.sicurezzaTitolo4')}}</h4>
            <p>{{__('ui.sicurezzaTesto2')}}</p>
          </div>
          <div class="col-12 col-lg-4 px-5">
            <i class="fa-solid fa-shield-halved fa-2x"></i>
            <h4 class="fs-3 mb-3 p-0 fw-semibold ">{{__('ui.sicurezzaTitolo5')}}</h4>
            <p>{{__('ui.sicurezzaTesto3')}}</p>
          </div>
          <div class="col-12 col-lg-3">
            <i class="fa-regular fa-font-awesome fa-2x"></i>
            <h4 class="fs-3 mb-3 p-0 fw-semibold ">{{__('ui.sicurezzaTitolo6')}}</h4>
            <p>{{__('ui.sicurezzaTesto4')}}</p>
          </div>
        </div>
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{url()->previous()}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>       
    </div>

</x-layout>