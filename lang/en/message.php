<?php
    return[
        // categorie
        'Elettronica'=>'Electronics',
        'Film/Serie tv'=>'Movie/TV series',
        'Finanza'=>'Finance',
        'Giardinaggio'=>'Gardening',
        'Libri/Magazine'=>'Book/Magazine',
        'Musica'=>'Music',
        'Per la casa'=>'Home products',
        'Sport'=>'Sport',
        'Abbigliamento'=>'Clothing',
        'Viaggi'=>'Travels',
        // messaggi dal controller
        'annuncioinserito'=>'POSTED ANNOUNCEMENT, AWAIT REVISION.',
        'annuncioconvalidato'=>'ANNOUNCEMENT VALIDATED',
        'annunciorifiutato'=>'ANNOUNCEMENT REJECTED',
        'candidaturainviata'=>'APPLICATION SUBMITTED',
        'seirevisore'=>'NOW YOU ARE REVISOR',
        'essereloggato'=>'YOU MUST BE LOGGED',
        'modificaannullata'=>'MODIFICATION CANCELLED',
    ];