<x-layout>
    <x-slot name="title">Presto - Mappa del sito</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container mb-2">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10 ">
            <h3 class="text-center display-5 mb-3 pt-4 mt-4  fw-semibold">{{__('ui.mappaTitolo')}}</h3>
            </div>
        </div>
        <div class="row justify-content-center my-2">  
          <div class="col-1 col-sm-1 d-flex align-self-center justify-content-end order-2 order-sm-2 order-lg-1 mt-3 mt-sm-3 mt-lg-0">
            <i class="scrollLeft fa-solid fa-circle-arrow-left fa-2x"></i>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa1.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">1 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa1.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-3 mb-3 p-0">{{__('ui.mappa1.2')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa13.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">2 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa2.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa2.2')}}</p>                  
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa2.3')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa2.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">3 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa3.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa3.2')}}</p>                  
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa3.3')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa3.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">4 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa4.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa4.2')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa4.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">5 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa5.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa5.2')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa5.3')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa5.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">6 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa6.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa6.2')}}</p>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa6.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">7 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa7.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa7.2')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa7.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">8 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa8.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa8.2')}}</p>                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa8.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">9 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa9.1')}}</p>                  
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa9.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa9.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">10 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa10.1')}}</p>                  
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa10.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa10.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">11 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa11.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa11.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa11.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">12 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa12.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa12.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa12.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">13 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-3 mb-3 p-0">{{__('ui.mappa13.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa13.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="/image/mappa14.jpg" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">14 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-4 mb-3 p-0">{{__('ui.mappa14.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-6 mb-3 p-0">{{__('ui.mappa14.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-lg-8 mySlides FADE order-1 order-sm-1 order-lg-2">
            <div class="card" style="max-width: 100%;">
              <div class="row g-0">
                <div class="col-12 col-sm-12 col-xl-6">
                  <img src="..." class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-12 col-sm-12 col-xl-6 d-flex flex-column justify-content-around">
                  <div class="card-title numbertext text-end">15 / 15</div>
                  <div class="card-body">
                    <p class="card-text text-dark fw-bold fs-3 mb-3 p-0">{{__('ui.mappa15.1')}}</p>
                    <p class="card-text text-dark fw-bold fs-5 mb-3 p-0">{{__('ui.mappa15.2')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-1 col-sm-1 d-flex align-self-center order-3 order-sm-3 order-lg-3 mt-3 mt-sm-3 mt-lg-0">
            <i class="scrollRight fa-solid fa-circle-arrow-right fa-2x"></i> 
          </div>
        </div>
        <div class="col-12 px-0 py-2 mx-0 justify-content-center d-flex align-items-start">
          <span class="dot">0</span> 
          <span class="dot">1</span> 
          <span class="dot">2</span> 
          <span class="dot">3</span> 
          <span class="dot">4</span> 
          <span class="dot">5</span> 
          <span class="dot">6</span> 
          <span class="dot">7</span> 
          <span class="dot">8</span> 
          <span class="dot">9</span> 
          <span class="dot">10</span> 
          <span class="dot">11</span> 
          <span class="dot">12</span> 
          <span class="dot">13</span>
          <span class="dot">14</span>  
        </div>
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{route('homePage')}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>        
    </div>
</x-layout>