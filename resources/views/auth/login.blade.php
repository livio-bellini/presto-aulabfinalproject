<x-layout>
    <x-slot name="title">Presto - Login</x-slot>
        <div class="container-fluid vh-100">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5 my-5">
              <div class="card bg-dark text-white" style="border-radius: 1rem;">
                <div class="card-body p-5 text-center">
                    <h2 class="fw-bold mb-2 text-uppercase">{{__('ui.login')}}</h2>
                    <p class="text-white-50 mb-5">{{__('ui.fraseLogin1')}}</p>
                    <form method="POST" action="{{route('login')}}">
                        @csrf
                        <div class="form-outline form-white mb-4">
                        <input type="email" name="email" id="typeEmailX" class="form-control form-control-sm" placeholder="email@email.com"/>
                        <label class="form-label" for="typeEmailX">Email</label>
                        </div>
                        <div class="form-outline form-white mb-4">
                        <input type="password" name="password" id="typePasswordX" class="form-control form-control-sm" placeholder="**********"/>
                        <label class="form-label" for="typePasswordX">{{__('ui.REGLOGpassword')}}</label>
                        </div>
                        <button class="btn btn-outline btn-sm px-5  yellow zoomButton fw-bold rounded fs-5 mb-1 text-uppercase" type="submit" style="color:black;">{{__('ui.login')}}</button>
                    </form>
                  <div>
                    <p class="mb-0">{{__('ui.fraseLogin2')}} <a href="{{route('register')}}" class="text-white-50 fw-bold">{{__('ui.linkAregistrazione')}}</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</x-layout>