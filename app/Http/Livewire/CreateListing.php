<?php

namespace App\Http\Livewire;

use App\Models\Listing;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use Livewire\WithFileUploads;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateListing extends Component
{
    use WithFileUploads;
    public $title;
    public $price;
    public $description;
    public $temporary_images;
    public $images = [];
    public $listing;
    public $category;

    protected $rules=[
        'title'=>'required|max:80',
        'price'=>'required|numeric',
        'description'=>'required|min:10',
        'category'=>'required',
        'temporary_images.*'=>'image|max:4096',
        'images.*'=>'image|max:4096'
    ];

    protected $messages=[
        'title.required'=>'Il nome dell\'articolo è obbligatorio',
        'title.max'=>'Il nome deve essere lungo massimo 20 caratteri',
        'price.required'=>'Il prezzo dell\'articolo è obbligatorio',
        'price.numeric'=>'Il prezzo deve essere un numero',
        'description.required'=>'Inserire una breve descrizione',
        'description.min'=>'La descrizione deve essere minimo di 10 parole',
        'category.required'=>'La categoria è obbligatoria',
        'temporary_images.*.image'=>'Il file deve essere un\'immagine',
        'temporary_images.*.max'=>'La dimensione del file consentita è massimo 4MB',
        'images.image'=>'Il file deve essere un\'immagine',
        'images.max'=>'La dimensione del file consentita è massimo 4MB'
    ];

    public function updatedTemporaryImages() {
        if ($this->validate([
            'temporary_images.*'=>'image|max:4096',
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }
    
    public function removeImage($key){
        if (in_array($key, array_keys($this->images))){
            unset($this->images[$key]);
        }
    }

    public function createListing(){

        $this->validate();

        $this->listing=Category::find($this->category)->listings()->create($this->validate());
        if (count($this->images)){
            foreach ($this->images as $image) {
                $newFileName = "listings/{$this->listing->id}";
                $newImage = $this->listing->images()->create(['path'=>$image->store($newFileName, 'public')]);

                //  decommentare per sprint 11/01 dispatch(new ResizeImage($newImage->path, 400, 400));

                // commentare per sprint 11/01
                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 400, 400),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                ])->dispatch($newImage->id);
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }
        
        $this->listing->user()->associate(Auth::user());
        $this->listing->save();
        $this->cleanForm();
        return redirect(route('allListing'))->with('message', __('message.annuncioinserito'));       
    }

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function cleanForm(){
        $this->title='';
        $this->price='';
        $this->description='';
        $this->temporary_images=[];
        $this->images = [];
        $this->listing='';
    }    

    public function render()
    {
        return view('livewire.create-listing');
    }

    
}
