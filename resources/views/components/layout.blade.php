<!DOCTYPE html>
<html lang="{{str_replace('', '-', app()->getLocale())}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title ?? '???'}}</title>
    {{-- icon tab --}}
    <link rel="icon" href="/image/logoPresto.png" type="image/x-icon">

    {{-- cdn fontawesom --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous"     referrerpolicy="no-referrer"/>   

    {{-- cdn googlefonts per font "Rajdhani" normale e grassetto --}}
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;700&display=swap" rel="stylesheet">

    {{-- cdn googlefonts per font "Roboto" --}}
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Righteous&family=Roboto:wght@300&display=swap" rel="stylesheet">
    
    {{-- cdn googlefonts per font "Play" normale e grassetto --}}
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Play:wght@400;700&display=swap" rel="stylesheet">

    {{-- cdn swiper --}}
    {{-- <link rel="stylesheet" href=“https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css”/> --}}

    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"/>
    
    
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    
    @livewireStyles
    
</head>
<body class="lightMode">

    <x-navbar/>

    @if(Route::currentRouteName() !== 'login' && Route::currentRouteName() !== 'register')
        @include('components.header')
    @endif

    {{$slot}}

    <i class="topBtnYellow d-none fa-solid fa-circle-arrow-up"></i>


    @if(Route::currentRouteName() !== 'login' && Route::currentRouteName() !== 'register')
        @include('components.footer')
    @endif
    
    @livewireScripts

    {{-- js swiper --}}
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
</body>
</html>