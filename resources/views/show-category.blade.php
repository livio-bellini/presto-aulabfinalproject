<x-layout>
    <x-slot name="title">Presto - {{$category->category}}</x-slot>
    <div class="container-fluid p-0">
      <div class="container my-0">
        <div class="row">
          <div class="col-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container-fluid bg-index-img">
        <div class="row vh-40 justify-content-center align-items-end">
          {{-- <div class="col-12 mt-5">
            <h1 class="text-center display-3 my-3 p-0 text-light fw-semibold text-uppercase font-title">{{__('message.'.$category->category)}}</h1> 
          </div>  --}}
          <div class="col-12 col-sm-12 col-md-8 col-lg-3 col-xl-2 mb-5"> 
            @if(Auth::check())
              <a class="btn yellow zoomButton fw-bold rounded" style="width:100%;" href="{{route('createListing')}}">{{__('ui.nuovo')}}</a>
            {{-- @else
              <a class="btn yellow zoomButton font-title rounded" style="width:100%; font-size:1rem" href="{{route('login')}}">Login</a> --}}
            @endif
          </div>
          <form class="d-block d-lg-none col-12 col-sm-12 col-md-8 mb-5" action="{{route('searchListings')}}" method="GET">
            <div class="row px-0 mx-0"> 
              <div class="col-10 px-0">
                  <input name="searched" class="form-control angleLeft" style="width:100%;" type="search" placeholder="{{__('ui.cerca')}}" aria-label="search"> 
              </div>
              <div class="col-2 px-0">
                  <button class="btn yellow  angleRight text-center px-0" style="width:100%;" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button> 
              </div>  
            </div>                  
          </form>
        </div>
      </div>
      <div class="container mt-4 my-0">
        <div class="row mb-5 px-2">
          <div class="col-12 mt-4 text-center">
            <h1 class="display-5 fw-semibold mt-2 p-0 text-uppercase">{{__('message.'.$category->category)}}:</h1> 
            <h2 class="fs-5 mt-2 mb-4 p-0">
              @if($category->id == 1)
                {{__('ui.fraseElettronica')}}
              @elseif($category->id == 2)
                {{__('ui.fraseFilm')}}
              @elseif($category->id == 3)
                {{__('ui.fraseFinanza')}}
              @elseif($category->id == 4)
                {{__('ui.fraseGiardinaggio')}}
              @elseif($category->id == 5)
                {{__('ui.fraseLibri')}}
              @elseif($category->id == 6)
                {{__('ui.fraseMusica')}}
              @elseif($category->id == 7)
                {{__('ui.fraseCasa')}}
              @elseif($category->id == 8)
                {{__('ui.fraseSport')}}
              @elseif($category->id == 9)
                {{__('ui.fraseAbbigliamento')}}
              @elseif($category->id == 10)
                {{__('ui.fraseViaggi')}}
              @endif
            </h2>
          </div>
          @forelse($category->listings as $listing)
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 px-0 justify-content-center d-flex">
              <div class="card shadow singleCard px-0 m-2">
                <div class="card-image text-center">
                  <img src="{{!$listing->images()->get()->isEmpty() ? $listing->images()->first()->getUrl(400,400) : "/image/default.png"}}" class="image-fluid">                
                </div>
                <div class="card-body">
                  <p class="card-heading text-dark fw-bold font-standard my-0 p-0 text-truncate d-inline-block" style="max-width:100%;">{{$listing->title}}</p>
                  <p class="card-title text-dark font-standard mb-0">{{__('message.'.$listing->category->category)}}</p>
                  <p class="card-text text-dark font-standard p-0">Inserito il: {{$listing->created_at->format('d/m/Y')}}</p>
                </div>
                <a href="{{route('showListing', compact('listing'))}}" class="card-button fw-bold text-decoration-none">{{__('ui.dettagli')}}</a>
              </div>
            </div>
          @empty
            <div class="box">
              <div class="col-12 text-center alert alert-warning">
                @if(Auth::check())
                  <p href="{{route('createListing')}}" class="fw-semibold mb-0 text-decoration-none text-dark">{{__('ui.noannunci')}}</p>
                @else
                  <p class="fw-semibold mb-0">{{__('ui.no')}}<a href="{{route('login')}}">{{__('ui.accedi')}}</a> {{__('ui.o')}} <a href="{{route('register')}}">{{__('ui.registrati')}}</a> {{__('ui.pubblicare')}}</p>
                @endif
              </div> 
            </div>           
          @endforelse
          <div class="col-12">
            {{$category->listings->links()}}   
          </div>
          
        </div>
      </div>
    </div>
</x-layout>