<header>
    <div class="container-fluid bg-header">
      <div class="row justify-content-center mx-auto headerBorder">
        @auth 
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold" href="{{route('createListing')}}">{{__('ui.inserisci')}}</a>        
        </div>
        @endauth
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold" href="{{route('whoPage')}}">{{__('ui.chi')}}</a>
        </div>
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold"  href="{{route('rulesPage')}}">{{__('ui.regole')}}</a>
        </div>
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold" href="{{route('writeMailBecome')}}">{{__('ui.lavora')}}</a>
        </div>
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold" href="{{route('tipsPage')}}">{{__('ui.consigli')}}</a>
        </div>
        <div class="col-12 col-sm-12 col-lg-2 text-center" style="width:auto;">
          <a class="text-decoration-none text-dark fw-bold" href="{{route('mapPage')}}">{{__('ui.mappa')}}</a>
        </div>
      </div>
    </div>
</header>
