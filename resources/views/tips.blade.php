<x-layout>
    <x-slot name="title">Presto - Vendi su Presto.it</x-slot>
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container my-4">
        <div class="row">
          <h1 class="text-center display-3 mb-3 p-0 fw-semibold text-uppercase font-title">{{__('ui.consigli')}}</h1>
          <div class="col-12 col-lg-8 gradient-custom-index2">
            <div class="card" style="width:100%;">
              <img src="/image/vendiSuPresto1.jpg" class="card-img-top" alt="Ragazza che fa shopping">
            </div>
            <div class="row">
              <div class="col-12 mt-3 d-flex justify-content-center">
                <div class="card mb-3" style="max-width: 540px;">
                  <div class="row g-0">
                    <div class="col-md-4">
                      <img src="/image/vendiSuPresto2.jpg" class="rounded-circle img-fluid rounded-start" alt="Persona che fotografa un gelato.">
                    </div>
                    <div class="col-md-8">
                      <div class="card-body">
                        <h5 class="card-title fw-bold text-dark fs-4">{{__('ui.consigliTitolo1')}}</h5>
                        <p class="card-text p-0 fs-6">{{__('ui.consigliTesto1')}}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 d-flex justify-content-center">
                <div class="card mb-3" style="max-width: 540px;">
                  <div class="row g-0">
                    <div class="col-md-4">
                      <img src="/image/vendiSuPresto3.jpg" class="rounded-circle img-fluid rounded-start" alt="Foto che mostra i soldi.">
                    </div>
                    <div class="col-md-8">
                      <div class="card-body">
                        <h5 class="card-title fw-bold text-dark fs-4">{{__('ui.consigliTitolo2')}}</h5>
                        <p class="card-text p-0 fs-6">{{__('ui.consigliTesto2')}}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 d-flex justify-content-center">
                <div class="card mb-3" style="max-width: 540px;">
                  <div class="row g-0">
                    <div class="col-md-4">
                      <img src="/image/vendiSuPresto4.jpg" class="rounded-circle img-fluid rounded-start" alt="Persona con lo smartphone.">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title fw-bold text-dark fs-4">{{__('ui.consigliTitolo3')}}</h5>
                          <p class="card-text p-0 fs-6">{{__('ui.consigliTesto3')}}</p>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-12 col-lg-4" id="boxEsR">
          <div class="card mb-4 opacity-0" style="width: 100%;">
            <div class="card-body text-end">
              <h5 class="card-title fw-bold text-uppercase text-dark fs-2">{{__('ui.consigliTitolo4')}}</h5>
              <p class="card-text fw-bold text-dark fs-3">{{__('ui.consigliTesto4')}}</p>
              <a href="{{route('safetyPage')}}" class="btn yellow my-1 fw-bold" style="color:black;">{{__('ui.continueCardButton')}}</a>
            </div>
          </div>
          <div class="card mb-4 opacity-0" style="width: 100%;">
            <img src="/image/vendiSuPresto5.jpg" class="ms-5 rounded-circle img-fluid card-img-top" alt="Foto dello smartphone." style="width:78%">
            <div class="card-body text-end">
              <h5 class="card-title fw-bold text-dark fs-4">{{__('ui.consigliTitolo5')}}</h5>
              <p class="card-text p-0 text-dark fs-6">{{__('ui.consigliTesto5')}}</p>
            </div>
          </div>
          <div class="card opacity-0" style="width: 100%;">
            <img src="/image/vendiSuPresto6.jpg" class="ms-5 rounded-circle img-fluid card-img-top" alt="Foto annunci." style="width:78%">
            <div class="card-body text-end">
              <h5 class="card-title fw-bold text-dark fs-4">{{__('ui.consigliTitolo6')}}</h5>
              <p class="card-text p-0 text-dark fs-6">{{__('ui.consigliTesto6')}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container mb-4">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-4 col-lg-2">
          <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{route('homePage')}}">{{__('ui.backButton')}}</a>
        </div>
      </div>
    </div>        
  </div>
</x-layout>