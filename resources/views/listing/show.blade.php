<x-layout>
    <x-slot name="title">Presto - {{$listing->title}} / {{$listing->category->category}}</x-slot>
    <div class="container-fluid">
        <div class="container my-2">
            <div class="row">
                <div class="col-4">
                    @if(session('message'))
                        <div class="alert alert-success hideAlert">
                            {{session('message')}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container my-0 px-0">
      <h3 class="text-center my-2 p-0 fw-semibold text-uppercase font-title" style="font-size:2.5rem;">{{__('ui.dettagliAnnuncio')}}</h3>
      <div class="row w-100 px-2 px-sm-0 px-md-0 mx-0 justify-content-center">
        <div class="col-12 col-sm-12 col-md-10 px-0 mt-2 gradient-custom-index2">
          <div class="row mx-0 px-0">
            <div class="col-12 col-sm-12 col-md-1 d-flex position-relative overflows px-0 d-none d-sm-none d-md-block"> 
            </div>
            <div class="col-12 col-sm-12 col-md-10 px-0 d-flex justify-content-center">
              {{-- INIZIO DELLA CARD ANNUNCIO --}}
              <div class="card showCard shadow rounded bg-card" style="width:1000px;">
                <div class="row g-0 mt-3">
                  <div class="col-md-7">
                    <div class="card-image rounded">
                      <div class="swiper mySwiper2">
                      @if($listing->images->isNotEmpty())
                        <div class="swiper-wrapper">
                          @foreach($listing->images as $image)
                            <div class="swiper-slide">
                              <img src="{{$image->getUrl(400,400)}}" class="image-fluid rounded" style="max-width:70%">
                            </div>
                          @endforeach
                        </div>
                      @else
                        <div class="swiper-wrapper">
                          <div class="swiper-slide">
                            <img src="/image/default.png" class="image-fluid rounded" style="width:100%">
                          </div>
                        </div>
                      @endif
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                      </div>
                      <div thumbsSlider="" class="swiper mySwiper">
                        @if($listing->images->isNotEmpty())
                          <div class="swiper-wrapper">
                            @foreach($listing->images as $image)
                              <div class="swiper-slide">
                                <img src="{{$image->getUrl(400,400)}}" class="image-fluid rounded" style="max-width:100%">
                              </div>
                            @endforeach
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="card-body">
                      <p class="card-text p-0 m-0 text-capitalize font-standard" style="font-size:1.5rem;">{{__('message.'. $listing->category->category)}}</p>
                      <hr class="text-dark">
                      <p class="card-text p-0 m-0 font-standard" style="font-size:1rem;">{{__('ui.dataArticolo')}}{{$listing->created_at->format('d/m/Y')}}</p>
                      <h3 class="card-title p-0 mt-5 fw-bold text-dark font-standard">{{$listing->title}}</h3>
                      <p class="card-text p-0 mt-2 fw-bold text-capitalize font-standard" style="color:blue; font-size:2rem;">{{$listing->price}}&euro;</p>
                    </div>
                  </div>
                </div>
                <div class="row mx-0 px-0 justify-content-center">
                  <div class="col-12">
                    <div class="card-body border border-dark rounded my-2">
                      <p class="card-text p-0 m-0 font-standard" style="font-size:1rem;">{{__('ui.autoreArticolo')}}<span class="text-dark fw-bold text-capitalize">{{$listing->user->name ?? ''}}</span></p>
                      <hr class="text-dark">
                      <h4 class="card-title p-0 mb-3 fw-bold text-dark font-standard">{{__('ui.descrizioneArticolo')}}</h4>
                      <p class="card-text p-0 m-0 text-dark font-standard" style="font-size:1rem;">{{$listing->description}}</p>
                    </div>
                  </div>
                  <div class="col-7 col-sm-4 col-lg-3 ">
                    <a href="{{url()->previous()}}" class="card-button fw-bold rounded text-decoration-none mb-2">{{__('ui.backButton')}}</a>
                  </div>
                </div>
              </div>
              {{-- FINE DELLA CARD ANNUNCIO --}}
            </div>
            <div class="col-12 col-sm-12 col-md-1 px-0 d-none d-sm-none d-md-block">
            </div>
            <div class="mb-5"></div>
          </div>
        </div>
      </div> 
    </div>
</x-layout>