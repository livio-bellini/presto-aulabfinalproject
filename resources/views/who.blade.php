<x-layout>
    <x-slot name="title">Presto - Chi siamo</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container mb-2">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10 ">
            <p class="text-center fs-5 my-0 pt-4 fw-semibold">Questo applicativo è stato ideato e scritto dal team "Lockheed Martin" dell'Hackademy60 del corso Full Stack Developer di Aulab.</p>
            <p class="my-0 text-center" style="font-size:0.8rem;">Il nome ed il marchio Aulab sono registrati.</p>
            <p class="mt-3 mb-0 text-start fs-6">La continua e progressiva ricerca di un design semplice ed accattivante da implementare all'applicativo Presto hanno permesso al team di scegliere la più adatta palette di colori, abbinata ad un uso efficace delle animazioni visive.</p>
            <p class="my-0 text-start fs-6">Il nome del team è stato scelto per rendere omaggio alla famosa azienda americana, produttrice del mitico aereo da combattimento F22 - Raptor.</p>
          </div>
        </div>
        <div class="row justify-content-center my-4">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 mt-2">
            <div class="card" style="width:100%;">
              <img src="/image/NicolaLupoli.jpg" class="card-img img" alt="Nicola">
              <div class="card-body">
                <h5 class="card-title text-dark">Nicola Lupoli</h5>
                <p class="card-text p-0">Diplomato all'istituto tecnico commerciale, appassionato di videogiochi e tecnologia.</p>
              </div>
              <div class="card-body pt-4">
                <i class="fa-brands fa-linkedin fa-2x" style="color:#1F487E"></i>
                <a class="card-text p-0" href="https://www.linkedin.com/in/nicola-lupoli-web-developer/">https://www.linkedin.com/in/nicola-lupoli-web-developer/</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 mt-2">
            <div class="card" style="width:100%;">
              <img src="/image/PaolaLattanzio.jpg" class="card-img img" alt="Paola">
              <div class="card-body">
                <h5 class="card-title text-dark">Paola Lattanzio</h5>
                <p class="card-text p-0">Diplomata all'istituto tecnico economico, settore turistico. Adora viaggiare, fare sport e fare web design.</p>
              </div>
              <div class="card-body">
                <i class="fa-brands fa-linkedin fa-2x" style="color:#1F487E"></i>
                <a class="card-text p-0" href="https://www.linkedin.com/in/paola-lattanzio-fullstackdeveloper/">https://www.linkedin.com/in/paola-lattanzio-fullstackdeveloper/</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 mt-2">
            <div class="card" style="width:100%;">
              <img src="/image/LetterioBorgosano.jpg" class="card-img img" alt="Letterio">
              <div class="card-body">
                <h5 class="card-title text-dark">Letterio Borgosano</h5>
                <p class="card-text p-0">Nato e cresciuto in Sicilia, trasferito a Milano da qualche anno. Ama raggiungere gli obbiettivi e mettersi in gioco. Appassionato di tecnologia e dei nuovi aggiornamenti.</p>
              </div>
              <div class="card-body">
                <i class="fa-brands fa-linkedin fa-2x" style="color:#1F487E"></i>
                <a class="card-text p-0" href="https://www.linkedin.com/in/letterio-webdeveloper">https://www.linkedin.com/in/letterio-webdeveloper</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 mt-2">
            <div class="card" style="width:100%;">
              <img src="/image/LivioBellini.jpg" class="card-img img" alt="Livio">
              <div class="card-body">
                <h5 class="card-title text-dark">Livio Bellini</h5>
                <p class="card-text p-0">Diplomato in elettronica e telecomunicazioni, appassionato di computer, musica, tecnologia e divulgazione scientifica. Adora suonare la chitarra elettrica a tutto volume fino a far vibrare i vetri delle finestre.</p>
              </div>
              <div class="card-body">
                <i class="fa-brands fa-linkedin fa-2x" style="color:#1F487E"></i>
                <a class="card-text p-0" href="https://www.linkedin.com/in/liviobellini">https://www.linkedin.com/in/liviobellini</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-12 col-md-10 ">
            <p class="my-0 text-start fs-6">Un ringraziamento speciale va agli insegnanti dell'Hackademy60: Alessandro Leo, Roberto Sasso, Nicola Menonna, Marco Insabato, Francesco Talomona.</p>
          </div>
        </div>
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{url()->previous()}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>    
    </div>
</x-layout>