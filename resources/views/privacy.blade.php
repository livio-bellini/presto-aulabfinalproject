<x-layout>
    <x-slot name="title">Presto - Condizioni e privacy</x-slot>
    <div class="container-fluid px-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-10">
            <h3 class="text-center display-4 mb-3 pt-4 mt-4  fw-semibold">{{__('ui.privacyTitolo1')}}</h3>
            </div>
        </div>
        <hr style="border: solid 2px var(--dark)">
        <div class="row justify-content-center my-2">  
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-user fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold">{{__('ui.privacyTitolo2')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.privacyTesto1')}}</p>
          </div>
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-pen fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.privacyTitolo3')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.privacyTesto2')}}</p>
          </div>
          <div class="col-12 col-sm-12 col-md-10">
            <i class="fa-solid fa-rotate-right fa-2x"></i>
            <h4 class="fs-2 mb-3 p-0 fw-semibold ">{{__('ui.privacyTitolo4')}}</h4>
            <p class="fs-3 mb-3 p-0">{{__('ui.privacyTesto3')}}</p>
          </div>
        </div>
        <hr style="border: solid 2px var(--dark)">
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{route('homePage')}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>        
    </div>
    </div>  
</x-layout>