<?php
    return[
        // categorie
        'Elettronica'=>'Electrónica',
        'Film/Serie tv'=>'Películas/series de televisión',
        'Finanza'=>'Finanzas',
        'Giardinaggio'=>'Jardinería',
        'Libri/Magazine'=>'Libros/Revistas',
        'Musica'=>'Música',
        'Per la casa'=>'Para la casa',
        'Sport'=>'Deporte',
        'Abbigliamento'=>'Ropa',
        'Viaggi'=>'Excursiones',
        // messaggi dal controller
        'annuncioinserito'=>'ANUNCIO INSERTADO, PENDIENTE DE REVISIÓN.',
        'annuncioconvalidato'=>'ANUNCIO VALIDADO',
        'annunciorifiutato'=>'ANUNCIO RECHAZADO',
        'candidaturainviata'=>'APLICACIÓN ENVIADA',
        'seirevisore'=>'AHORA ERES AUDITOR',
        'essereloggato'=>'DEBE ESTAR CONECTADO',
        'modificaannullata'=>'MODIFICACIÓN ANULADA',
    ];