<x-layout>
    <x-slot name="title">Presto - Annunci</x-slot>
    <div class="container-fluid p-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container-fluid bg-index-img ">
        <div class="row vh-40 justify-content-center align-items-end">
          {{-- <div class="col-12 mt-5">
            <h1 class="text-center display-3 p-0 text-light fw-semibold text-uppercase font-title">{{__('ui.index')}}</h1> 
          </div>  --}}
          <div class="col-12 col-sm-12 col-md-8 col-lg-3 col-xl-2 mb-5"> 
            @if(Auth::check())
              <a class="btn yellow zoomButton fw-bold rounded" style="width:100%;" href="{{route('createListing')}}">{{__('ui.nuovo')}}</a>
            {{-- @else
              <a class="btn yellow zoomButton font-title rounded" style="width:100%; font-size:1rem" href="{{route('login')}}">Login</a> --}}
            @endif
          </div>
          <form class="d-block d-lg-none col-12 col-sm-12 col-md-8 mb-5" action="{{route('searchListings')}}" method="GET">
            <div class="row px-0 mx-0"> 
              <div class="col-10 px-0">
                  <input name="searched" class="form-control angleLeft" style="width:100%;" type="search" placeholder="{{__('ui.cerca')}}" aria-label="search"> 
              </div>
              <div class="col-2 px-0">
                  <button class="btn yellow  angleRight text-center px-0" style="width:100%;" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button> 
              </div>  
            </div>                  
          </form>
        </div>
      </div>  
      <div class="container my-0"> 
        @if(session('error'))
          <div class="alert alert-danger hideAlert">
            {{session('error')}}
          </div>
        @endif  
           
        <div class="row px-0 justify-content-center">
          <div class="col-12 mt-5 text-center">
            <h1 class="display-5 fw-semibold mt-2 mb-4 p-0 text-uppercase">{{__('ui.index')}}:</h1> 
          </div>
          <div class="col-12 col-sm-12 col-md-10 col-lg-8 px-0 gradient-custom-index2">
            <div class="row px-0 mx-0">
              
              <div class="col-12 col-sm-12 col-md-4 d-flex position-relative overflows col-lg-3 px-0 d-none d-sm-none d-md-block"> 
                <h1 class="font-title rotationStyling">Presto.it</h1>
              </div>
              <div class="col-12 col-sm-12 col-md-7 col-lg-8 px-2">
                @forelse($listings as $listing)                        
                  <div class="card mb-3 rounded">
                    <div class="row g-0">
                      <div class="col-10 col-sm-4 col-md-3">
                        <img src="{{!$listing->images()->get()->isEmpty() ? $listing->images()->first()->getUrl(400,400) : "/image/default.png"}}" style="max-width:100%;" class="image-fluid rounded" alt="Foto dell'annuncio.">
                      </div>
                      <div class="col-12 col-sm-6 col-md-7 order-3 order-sm-2">
                        <div class="card-body overflow-x-auto py-0">
                          <p class="card-heading text-dark fw-bold font-standard my-0 p-0 text-truncate d-inline-block" style="max-width:100%;">{{$listing->title}}</p>
                          <p class="card-title text-dark font-standard mb-0">{{__('message.'. $listing->category->category)}}</p>
                          <p class="card-text text-dark fw-bold font-standard p-0">Prezzo: {{$listing->price}}&euro;</p>
                        </div>
                      </div>
                      <div class="col-2 col-sm-2 col-md-2 d-flex justify-content-end order-2 order-sm-3 px-1 py-1">
                        <a href="{{route('showListing', compact('listing'))}}" class="card-button rounded text-decoration-none" style="border-radius:0px; width:40px; height:40px;"><i class="fa-solid fa-info"></i></a>
                      </div>
                    </div>
                  </div>
                @empty
                  <div class="col-12 my-0 alert alert-warning d-flex justify-content-center align-items-center" style="height:100px;">
                    <p class="my-0 fw-semibold">{{__('ui.noannunci')}}</p>
                  </div>  
                @endforelse
              </div>
              <div class="col-12 col-sm-12 col-md-1 col-lg-1 px-0 d-none d-sm-none d-md-block">
              </div>
              <div class="col-12">
                {{$listings->links()}}   
              </div>
              <div class="h-50"></div>
            </div>
          </div>
        </div>                         
      </div>                
    </div>
</x-layout>