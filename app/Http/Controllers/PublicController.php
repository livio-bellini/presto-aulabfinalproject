<?php

namespace App\Http\Controllers;

use App\Models\Listing;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home(){
        $listings = Listing::where('is_accepted', true)->orderBy('created_at', 'desc')->paginate(8);
        return view('welcome', compact('listings'));
    }

    public function show(Category $category) {
        $category->listings = Listing::where('category_id', $category->id)->where('is_accepted', true)->orderBy('created_at', 'desc')->paginate(10);
        return view('show-category', compact('category'));
    }

    public function searchLstngs(Request $request){
        $listings = Listing::search($request -> searched)->where('is_accepted', true)->paginate(25);
        return view('listing.index', compact('listings'));
    }

    public function who(){
        return view('who');
    }

    public function tips(){
        return view('tips');
    }

    public function safety(){
        return view('safety');
    }

    public function ambient(){
        return view('ambient');
    }

    public function rules(){
        return view('rules');
    }

    public function privacy(){
        return view('privacy');
    }

    public function map(){
        return view('map');
    }

    public function language($lang){
        session()->put('locale', $lang);
        switch($lang){
          case 'it':
            return redirect()->back()->with('message', 'LINGUA SELEZIONATA: ITALIANO');
            break;
          case 'gb':
            return redirect()->back()->with('message', 'LANGUAGE SELECTED: ENGLISH');
            break;
          case 'es':
            return redirect()->back()->with('message', 'IDIOMA SELECCIONADO: ESPAÑOL');
            break;  
          default:
            return redirect()->back()->with('message', 'LINGUA SELEZIONATA: ITALIANO');
            break;
        }    
    }
}

