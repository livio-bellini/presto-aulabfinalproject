<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


        $categories = ['Elettronica','Film/Serie tv','Finanza','Giardinaggio','Libri/Magazine','Musica','Per la casa', 'Sport','Abbigliamento','Viaggi'];

        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'category'=>$category,
            ]);
        }
    }

}
