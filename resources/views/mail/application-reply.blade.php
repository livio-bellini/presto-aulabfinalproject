<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail di risposta</title>
</head>
<body>
    <h1>RICEZIONE CANDIDATURA</h1>
    <p>Ciao {{$user->name}}, abbiamo ricevuto la tua candidatura come revisore annunci.
    Se l'esito sarà positivo, verrai ricontattato al più presto su questo indirizzo email.</p>
    <p>Buona fortuna!!</p>
</body>
</html>
