<x-layout>
    <x-slot name="title">Presto - Per l'ambiente</x-slot>
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-4">
            @if(session('message'))
              <div class="alert alert-success hideAlert">
                {{session('message')}}
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="container my-4">
        <div class="row">
          <div class="col-12 col-sm-12 col-xl-6">
            <img class="image-fluid" src="/image/Ambiente1.jpg" style="width:100%" alt="Mani che avvolgono il mondo.">
          </div>
          <div class="col-12 col-sm-12 col-xl-6">
            <h3 class="font-title text-center">{{__('ui.ambientTitle1')}}</h3>
            <p class="font-standard" style="font-size:1.2rem;">{{__('ui.ambientText1')}}</p>
            <h3 class="font-title text-center mt-5">{{__('ui.ambientTitle2')}}</h4>
            <p class="font-standard" style="font-size:1.2rem;">{{__('ui.ambientText2')}}</p>                   
          </div>
        </div>
      </div>
      <div class="container mb-4">
        <div class="row">
          <h3 class="font-title text-center mb-4">{{__('ui.ambientTitle3')}}</h2>
          <div class="col-12 col-sm-6 col-lg-3 d-flex justify-content-center">
            <div class="card" style="width: 16rem;">
              <div class="card-body text-center">
                <i class="fa-solid text-center fa-fan" style="font-size:8rem; color:var(--green);"></i>
                <h5 class="card-title" style="color:var(--green);">{{__('ui.ambientCard1')}}</h5>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 d-flex justify-content-center">
            <div class="card" style="width: 16rem;">
              <div class="card-body text-center">
                <i class="fa-solid  fa-arrow-trend-down" style="font-size:8rem; color:var(--green);"></i>                        
                <h5 class="card-title" style="color:var(--green);">{{__('ui.ambientCard2')}}</h5>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-lg-3 d-flex justify-content-center">
            <div class="card" style="width: 16rem;">
              <div class="card-body text-center"> 
                <i class="fa-solid text-center fa-recycle" style="font-size:8rem; color:var(--green);"></i>
                <h5 class="card-title" style="color:var(--green);">{{__('ui.ambientCard3')}}</h5>
              </div>
            </div>            
          </div>
          <div class="col-12 col-sm-6 col-lg-3 d-flex justify-content-center">
            <div class="card" style="width: 16rem;">
              <div class="card-body text-center">
                <i class="fa-solid text-center fa-leaf" style="font-size:8rem; color:var(--green);"></i>
                <h5 class="card-title" style="color:var(--green);">{{__('ui.ambientCard4')}}</h5>
              </div>
            </div>            
          </div>
        </div>
      </div>
      <div class="container mb-4">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-4 col-lg-2">
            <a class="btn yellow zoomButton fw-bold" style="width:100%; font-size:1rem" href="{{url()->previous()}}">{{__('ui.backButton')}}</a>
          </div>
        </div>
      </div>
    </div>
</x-layout>